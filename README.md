# TiCamShader Demo

Demo App for Podpro's Lens Visualizer using the TiCamShader module.


![Screenshot](https://img.skitch.com/20111110-twhufm1kxr1ymass6r5d9mfjip.jpg)


## Requirements

-   com.semanticpress.ticamshader iOS module

## License

@authors	
		
-   Terry Martin <martin@semanticpress.com>
-   Curtis Duhn <curtis.duhn@semanticpress.com>

Copyright (c) 2011, Semantic Press, Inc. <http://www.semanticpress.com>
Titanium is Copyright (c) 2009-2010 by Appcelerator, Inc. All Rights Reserved.
Appcelerator, Appcelerator Titanium and associated marks and logos are 
trademarks of Appcelerator, Inc. 


@license    

Titanium is licensed under the Apache Public License (Version 2). Please
see the LICENSE file for the full license.

MIT License
Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
and associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial 
portions of the Software. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

