(function() {
	App.ui = {
	  win: false,
	  view1:false,
	  view2:false
	};
	
	
	App.ui.createWindow = function() {
		
		var win = Ti.UI.createWindow({
			backgroundColor:'#fff',
			orientationModes:[
			  Titanium.UI.PORTRAIT, 
			  Titanium.UI.UPSIDE_PORTRAIT
			]
		});
		
		App.ui.view1 = (function(){
			// Only use modules for a specific platform
			return require('views/ui.'+App.osname).load();
		})();
		
		App.ui.view1.addEventListener('click',function() {
			// Dynamically Create Lens
			var LensView = require('views/ui.'+App.osname+'.lens').load({
				color:'amber', // <-- this is the selected color
				close:function(){
					Ti.API.info('Closing Lens Visualizer');
					LensView.close();
					LensView = null; // <-- be sure to clean up this proxy object
				}
			});
			
			LensView.open();
		});
			
		
		// Add Views
		win.add(App.ui.view1);
		
		return win;
		
	};
	
	
	
})();
