var ticamshader = require('com.semanticpress.ticamshader');
Ti.API.info("module is => " + ticamshader);

var colors = {
	'clear' : '#ffffff',
	'yellow': '#ffff6b',
	'amber': '#ffcd85',
	'red-ice': '#c8ffff',
	'blue-lagoon': '#c5ffa1',
	'blue-amber': '#ffde94',
	'pink-sq': '#ffbbc3',
	'silver-rose': '#ffabb2',
	'gold-chrome': '#ffde94',
	'silver-amber': '#ffe8a9',
	'blue-fusion': '#f0b87b',
	'red-solex': '#da954a',
	'green-solex': '#da954a',
	'blue-solex': '#ffcc61',
	'silver-solex': '#d9a737',
	'dark-smoke': '#949994'
};

var load = function(_args) {
	var WIN = Titanium.UI.createWindow({  
    	backgroundColor:'#000'
	});
	
	var camView = ticamshader.createCamView({
		top:80,
		height:380
	});
	
	camView.setColor(colors[_args.color]); // use hex value
	
	var overlay = Titanium.UI.createView({  
    	backgroundImage:'images/Lens.overlay.iphone.png'
	});
	var overlay_active = Ti.UI.createView({
		bottom:3,
		left:15,
		width:155,
		height:121,
		backgroundImage:'images/Lens_Active_Indicator.png'
	});
	var overlay_active_image = Ti.UI.createView({backgroundImage:'images/tints/Graybird_Tints_'+_args.color+'.png',width:84,height:90});
	
	var menu = Ti.UI.createScrollView({
		top:60,
		height:60,
		width:277,
		contentWidth:Object.keys(colors).length * 80,
		contentHeight: 60,
		showHorizontalScrollIndicator:false
	});
	                                
	var colorIndex = 0;
	for (var colorName in colors) {
		var swatch = Ti.UI.createView({
			colorName:colorName,
			backgroundColor: colors[colorName],
			top: 20,
			left: colorIndex * 70 + 20,
			width: 40,
			height: 40,
			borderWidth: 1,
			borderColor: 'black'
		});
		swatch.addEventListener('click', function(e) {
			Ti.API.info('Updating to color: '+e.source.colorName);
			camView.setColor(e.source.backgroundColor);
			// Update image
			overlay_active_image.backgroundImage = 'images/tints/Graybird_Tints_'+e.source.colorName+'.png';
		});
		menu.add(swatch);
		colorIndex++;
	}
	
	// Create Close button
	btnClose = Ti.UI.createButton({
		top:6,
		left:9,
		title:'',
		backgroundImage:'images/btn.home.jpg',
		width:44,
		height:30
	});
	btnClose.addEventListener('click',_args.close);	
	
	
	// Add Views
	overlay_active.add(overlay_active_image);
	
	WIN.add(camView);
	WIN.add(overlay);
	WIN.add(overlay_active);
	WIN.add(menu);
	WIN.add(btnClose);
	
	return WIN;

};

exports.load = load;
