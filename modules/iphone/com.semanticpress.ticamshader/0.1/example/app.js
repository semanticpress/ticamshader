var ticamshader = require('com.semanticpress.ticamshader');
Ti.API.info("module is => " + ticamshader);

var colors = {
	'clear' : '#ffffff',
	'yellow': '#ffff6b',
	'amber': '#ffcd85',
	'red-ice': '#c8ffff',
	'blue-lagoon': '#c5ffa1',
	'blue-amber': '#ffde94',
	'pink-sq': '#ffbbc3',
	'silver-rose': '#ffabb2',
	'gold-chrome': '#ffde94',
	'silver-amber': '#ffe8a9',
	'blue-fusion': '#f0b87b',
	'red-solex': '#da954a',
	'green-solex': '#da954a',
	'blue-solex': '#ffcc61',
	'silver-solex': '#d9a737',
	'dark-smoke': '#949994'
};

var views = [];
for (var colorName in colors) {
	if (colorName) {
		Ti.API.info('Color for ' + colorName + ' is ' + colors[colorName]);
		var pageView = Ti.UI.createView();
		// Set the camShaderColor for each view you add to the TiUIScrollableView
		pageView.camShaderColor = colors[colorName];
		var footerView = Ti.UI.createView({
			bottom:0,
			height:60,
			backgroundColor:'white',
			borderColor:'#EEEEEE',
			borderWidth:1
		});
		var swatchView = Ti.UI.createView({
			width:40,
			height:40,
			top:10,
			right:10,
			backgroundColor:colors[colorName],
			borderColor:'#DDDDDD',
			borderWidth:1
		});
		var label = Ti.UI.createLabel({
			text:colorName,
			left:10,
			width:250
		});
		footerView.add(label);
		footerView.add(swatchView);
		pageView.add(footerView);
		views.push(pageView);
	}
}

var scrollableView = ticamshader.createScrollableView({
	views:views
});
Ti.API.info("scrollableView is => " + scrollableView);

var camView = scrollableView.camView;
Ti.API.info("camView is => " + camView);

var win1 = Titanium.UI.createWindow({  
    backgroundColor:'#000'
});

camView.top = 0;
camView.bottom = 60;

win1.add(camView);
win1.add(scrollableView);
win1.open();


